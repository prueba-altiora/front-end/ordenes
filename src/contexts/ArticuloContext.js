import React, { createContext, useReducer } from 'react';
import {
    OBTENER_ARTICULO,
    OBTENER_ARTICULOS,
    REGISTRAR_ARTICULO,
    ACTUALIZAR_ARTICULO,
    ELIMINAR_ARTICULO,
  } from "../constantes/ActionTypes";
  import ArticuloReducer from "../reducer/ArticuloReducer";
  import Axios from "axios";
  import Swal from "sweetalert2";
  
  export const ArticuloContext = createContext();
  
  export const ArticuloContextProvider = (props) => {
    const initialState = {
      articulosList: [],
      articuloActual: null,
    };
    const [state, dispatch] = useReducer(ArticuloReducer, initialState);
  
    const obtenerArticulos = async () => {
      try {
        const resultado = await Axios.get("/articulo/listar");
  
        dispatch({
          type: OBTENER_ARTICULOS,
          payload: resultado.data,
        });
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "No se pudo obtener los articulos",
          toast: true,
        });
      }
    };
  
    const registrarArticulo = async (articulo) => {
      await Axios.post("/articulo/crear", articulo)
        .then((response) => {
          if (response.status === 200) {
            dispatch({
              type: REGISTRAR_ARTICULO,
              payload: response.data,
            });
            Swal.fire({
              icon: "success",
              title: "Correcto",
              text: "Articulo registrado correctamente.",
              toast: true,
            });
          }
        })
        .catch((error) => {
          if (error.response) {
            Swal.fire({
              icon: "error",
              title: "Error",
              text: error.response.data.apierror.mensajeSistema,
              toast: true,
            });
          }
        });
    };
  
    const obtenerArticulo = (articulo) => {
      dispatch({
        type: OBTENER_ARTICULO,
        payload: articulo,
      });
    };
  
    const actualizarArticulo = async (articulo) => {
      await Axios.put("/articulo/actualizar", articulo)
        .then((response) => {
          if (response.status >= 200 && response.status <= 300) {
            dispatch({
              type: ACTUALIZAR_ARTICULO,
              payload: response.data,
            });
            Swal.fire({
              icon: "success",
              title: "Correcto",
              text: "Articulo actualizado correctamente.",
              toast: true,
            });
          }
        })
        .catch((error) => {
          if (error.response) {
            Swal.fire({
              icon: "error",
              title: "Error",
              text: error.response.data.apierror.mensajeSistema,
              toast: true,
            });
          }
        });
    };
  
    const eliminarArticulo = async (idArticulo) => {
      Swal.fire({
        title: "¿Desea continuar?",
        text: "Se eliminará el articulo seleccionado",
        icon: "question",
        showCancelButton: true,
        confirmButtonText: "Si, eliminar",
      }).then(async (result) => {
        if (result.value) {
          await Axios.delete(`/articulo/eliminar/${idArticulo}`)
            .then((response) => {
              if (response.status >= 200 && response.status <= 300) {
                dispatch({
                  type: ELIMINAR_ARTICULO,
                  payload: idArticulo,
                });
              }
            })
            .catch((error) => {
              if (error.response) {
                Swal.fire({
                  icon: "error",
                  title: "Error",
                  text: error.response.data.apierror.mensajeSistema,
                  toast: true,
                });
              }
            });
        }
      });
    };
  
    return (
      <ArticuloContext.Provider
        value={{
          articulosList: state.articulosList,
          articuloActual: state.articuloActual,
          obtenerArticulos,
          registrarArticulo,
          obtenerArticulo,
          actualizarArticulo,
          eliminarArticulo,
        }}
      >
        {props.children}
      </ArticuloContext.Provider>
    );
  };
  