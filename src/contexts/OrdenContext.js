import React, { createContext, useReducer } from 'react';

import {
    OBTENER_ORDENES,
    OBTENER_ORDEN,
  } from "../constantes/ActionTypes";
  import OrdenReducer from "../reducer/OrdenReducer";
  import Swal from "sweetalert2";
import ClientOrden from '../middlewares/ClientOrden';
  
  export const OrdenContext = createContext();
  
  export const OrdenContextProvider = (props) => {
    const initialState = {
      ordenesList: [],
      ordenActual: null,
    };
    const [state, dispatch] = useReducer(OrdenReducer, initialState);
  
    const obtenerOrdenes = () => {
        console.log(ClientOrden)
        ClientOrden.get("/orden/listar").then((resultado)=>{
          dispatch({
            type: OBTENER_ORDENES,
            payload: resultado.data,
          });
        }).catch((error) => {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: "No se pudo obtener las ordenes",
            toast: true,
          });
        });
    };
  
    const obtenerOrden = (orden) => {
      dispatch({
        type: OBTENER_ORDEN,
        payload: orden,
      });
    };
  
    return (
      <OrdenContext.Provider
        value={{
          ordenesList: state.ordenesList,
          ordenActual: state.ordenActual,
          obtenerOrdenes,
          obtenerOrden,
        }}
      >
        {props.children}
      </OrdenContext.Provider>
    );
  };
  