import React, { createContext, useReducer } from "react";
import {
  OBTENER_CLIENTE,
  OBTENER_CLIENTES,
  REGISTRAR_CLIENTE,
  ACTUALIZAR_CLIENTE,
  ELIMINAR_CLIENTE,
} from "../constantes/ActionTypes";
import ClienteReducer from "../reducer/ClienteReducer";
import Axios from "axios";
import Swal from "sweetalert2";

export const ClienteContext = createContext();

export const ClienteContextProvider = (props) => {
  const initialState = {
    clientesList: [],
    clienteActual: null,
  };
  const [state, dispatch] = useReducer(ClienteReducer, initialState);

  const obtenerClientes = async () => {
    try {
      const resultado = await Axios.get("/cliente/listar");

      dispatch({
        type: OBTENER_CLIENTES,
        payload: resultado.data,
      });
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "No se pudo obtener los clientes",
        toast: true,
      });
    }
  };

  const registrarCliente = async (cliente) => {
    await Axios.post("/cliente/crear", cliente)
      .then((response) => {
        if (response.status === 200) {
          dispatch({
            type: REGISTRAR_CLIENTE,
            payload: response.data,
          });
          Swal.fire({
            icon: "success",
            title: "Correcto",
            text: "Cliente registrado correctamente.",
            toast: true,
          });
        }
      })
      .catch((error) => {
        if (error.response) {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: error.response.data.apierror.mensajeSistema,
            toast: true,
          });
        }
      });
  };

  const obtenerCliente = (cliente) => {
    dispatch({
      type: OBTENER_CLIENTE,
      payload: cliente,
    });
  };

  const actualizarCliente = async (cliente) => {
    await Axios.put("/cliente/actualizar", cliente)
      .then((response) => {
        if (response.status >= 200 && response.status <= 300) {
          dispatch({
            type: ACTUALIZAR_CLIENTE,
            payload: response.data,
          });
          Swal.fire({
            icon: "success",
            title: "Correcto",
            text: "Cliente actualizado correctamente.",
            toast: true,
          });
        }
      })
      .catch((error) => {
        if (error.response) {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: error.response.data.apierror.mensajeSistema,
            toast: true,
          });
        }
      });
  };

  const eliminarCliente = async (identificacion) => {
    Swal.fire({
      title: "¿Desea continuar?",
      text: "Se eliminará el cliente seleccionado",
      icon: "question",
      showCancelButton: true,
      confirmButtonText: "Si, eliminar",
    }).then(async (result) => {
      if (result.value) {
        await Axios.delete(`/cliente/eliminar/${identificacion}`)
          .then((response) => {
            if (response.status >= 200 && response.status <= 300) {
              dispatch({
                type: ELIMINAR_CLIENTE,
                payload: identificacion,
              });
            }
          })
          .catch((error) => {
            if (error.response) {
              Swal.fire({
                icon: "error",
                title: "Error",
                text: error.response.data.apierror.mensajeSistema,
                toast: true,
              });
            }
          });
      }
    });
  };

  return (
    <ClienteContext.Provider
      value={{
        clientesList: state.clientesList,
        clienteActual: state.clienteActual,
        obtenerClientes,
        registrarCliente,
        obtenerCliente,
        actualizarCliente,
        eliminarCliente,
      }}
    >
      {props.children}
    </ClienteContext.Provider>
  );
};
