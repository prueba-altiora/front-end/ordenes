import { ACTUALIZAR_CLIENTE, ELIMINAR_CLIENTE, OBTENER_CLIENTE, OBTENER_CLIENTES, REGISTRAR_CLIENTE } from "../constantes/ActionTypes";

// eslint-disable-next-line
export default (state,action) => {
    switch(action.type){
        case OBTENER_CLIENTES:
            return {
                ...state,
                clientesList: action.payload
            };
            case REGISTRAR_CLIENTE:
                return{
                    ...state,
                    clientesList: [...state.clientesList,action.payload]
                };
            case OBTENER_CLIENTE:
                return{
                    ...state,
                    clienteActual: action.payload
                };
            case ACTUALIZAR_CLIENTE:
                return{
                    ...state,
                    clientesList : state.clientesList.map(
                        cliente => cliente.identificacion === action.payload.identificacion ? action.payload:cliente
                    )
                }
            case ELIMINAR_CLIENTE:
                return{
                    ...state,
                    clientesList : state.clientesList.filter( cliente=> cliente.identificacion!== action.payload)
                    
                }
        default:
            return state;
    }
}