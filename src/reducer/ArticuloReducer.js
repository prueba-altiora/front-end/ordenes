import { ACTUALIZAR_ARTICULO, ELIMINAR_ARTICULO, OBTENER_ARTICULO, OBTENER_ARTICULOS, REGISTRAR_ARTICULO } from "../constantes/ActionTypes";

// eslint-disable-next-line
export default (state,action) => {
    switch(action.type){
        case OBTENER_ARTICULOS:
            return {
                ...state,
                articulosList: action.payload
            };
            case REGISTRAR_ARTICULO:
                return{
                    ...state,
                    articulosList: [...state.articulosList,action.payload]
                };
            case OBTENER_ARTICULO:
                return{
                    ...state,
                    articuloActual: action.payload
                };
            case ACTUALIZAR_ARTICULO:
                return{
                    ...state,
                    articulosList : state.articulosList.map(
                        articulo => articulo.idArticulo === action.payload.idArticulo ? action.payload:articulo
                    )
                }
            case ELIMINAR_ARTICULO:
                return{
                    ...state,
                    articulosList : state.articulosList.filter( articulo=> articulo.idArticulo!== action.payload)
                    
                }
        default:
            return state;
    }
}