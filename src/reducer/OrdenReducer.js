import { OBTENER_ORDEN, OBTENER_ORDENES} from "../constantes/ActionTypes";

// eslint-disable-next-line
export default (state,action) => {
    switch(action.type){
        case OBTENER_ORDENES:
            return {
                ...state,
                ordenesList: action.payload
            };
            case OBTENER_ORDEN:
                return{
                    ...state,
                    ordenActual: action.payload
                };
        default:
            return state;
    }
}