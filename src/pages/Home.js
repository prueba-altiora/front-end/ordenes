import React from "react";
import Layout from "../components/commons/Layotu";

const Home = () => {
  return (
    <Layout>
      <div className="panel">
        <div className="panel-heading">Home</div>
        <div className="box">
          <img src="logo.png" alt="Logo" />
        </div>
      </div>
    </Layout>
  );
};

export default Home;
