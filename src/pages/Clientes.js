import React from "react";
import BotonCrearCliente from "../components/clientes/BotonCrearCliente";
import FormularioCliente from "../components/clientes/FormularioCliente";
import TablaCliente from "../components/clientes/TablaCliente";
import Layout from "../components/commons/Layotu";
import Modal from "../components/commons/Modal";
import {ClienteContextProvider} from "../contexts/ClienteContext"

const Clientes = () => {
  return (
    <Layout>
      <ClienteContextProvider>
        <div className="panel">
          <div className="panel-heading">Clientes</div>
          <div className="box">
            <BotonCrearCliente />
            <TablaCliente />
          </div>
        </div>
        <Modal>
          <FormularioCliente />
        </Modal>
      </ClienteContextProvider>
    </Layout>
  );
};

export default Clientes;
