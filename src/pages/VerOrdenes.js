import React from "react";
import Layout from "../components/commons/Layotu";
import Modal from "../components/commons/Modal";
import FormularioDetalle from "../components/ordenes/FormularioDetalle";
import TablaOrden from "../components/ordenes/TablaOrden";
import { OrdenContextProvider } from "../contexts/OrdenContext";

const VerOrdenes = () => {
  return (
    <Layout>
      <OrdenContextProvider>
        <div className="panel">
          <div className="panel-heading">Visualizar Órdenes</div>
          <div className="box">
            <TablaOrden />
          </div>
        </div>
        <Modal>
          <FormularioDetalle />
        </Modal>
      </OrdenContextProvider>
    </Layout>
  );
};

export default VerOrdenes;
