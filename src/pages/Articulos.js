import React from 'react';
import BotonCrearArticulo from "../components/articulos/BotonCrearArticulo";
import FormularioArticulo from "../components/articulos/FormularioArticulo";
import TablaArticulo from "../components/articulos/TablaArticulo";
import Layout from "../components/commons/Layotu";
import Modal from "../components/commons/Modal";
import {ArticuloContextProvider} from "../contexts/ArticuloContext"

const Articulos = () => {
    return ( 
    <Layout>
        <ArticuloContextProvider>
          <div className="panel">
            <div className="panel-heading">Artículos</div>
            <div className="box">
              <BotonCrearArticulo />
              <TablaArticulo />
            </div>
          </div>
          <Modal>
            <FormularioArticulo />
          </Modal>
        </ArticuloContextProvider>
      </Layout> );
}
 
export default Articulos;