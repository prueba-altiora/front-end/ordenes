import React, { useState } from 'react';
import Layout from '../components/commons/Layotu';
import BusquedaCliente from '../components/ordenes/BusquedaCliente';
import Detalles from '../components/ordenes/Detalles';

const Ordenes = () => {

    const [identificacionCliente, setIdentificacionCliente] = useState("");

    return (  
        <Layout>
            <div className="panel">
                <div className="panel-heading">Órdenes</div>
                <BusquedaCliente onSelect={setIdentificacionCliente}>
                </BusquedaCliente>
                
                
                <Detalles identificacionCliente={identificacionCliente}></Detalles>
            </div>
        </Layout>
    );
}
 
export default Ordenes;