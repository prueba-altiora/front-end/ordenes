import React, { useContext } from "react";
import { ArticuloContext } from "../../contexts/ArticuloContext";
import { ModalContext } from "../../contexts/ModalContext";

const BotonCrearArticulo = () => {

    const {setModalTitle, setShowModal}= useContext(ModalContext)

    const {obtenerArticulo} = useContext(ArticuloContext)
    const abrirModalCrearArticulo = () =>{
        setModalTitle('Nuevo Articulo');
        setShowModal(true);
        obtenerArticulo(null);
    }

  return (
    <div className="container">
      <button className="button is-small is-primary" onClick={()=>abrirModalCrearArticulo()}>
        <span className="icon is-small">
          <i className="fas fa-plus-square"></i>
        </span>
        <span>Registrar Articulo</span>
      </button>
    </div>
  );
};

export default BotonCrearArticulo;
