import React, { useContext, useState, useEffect } from "react";
import { ArticuloContext } from "../../contexts/ArticuloContext";
import { ModalContext } from "../../contexts/ModalContext";

const FormularioArticulo = () => {
  const { setShowModal, setModalTitle } = useContext(ModalContext);
  const {
    registrarArticulo,
    articuloActual,
    obtenerArticulo,
    actualizarArticulo,
  } = useContext(ArticuloContext);

  const articuloDefault = {
    idArticulo:"",
    codigo: "",
    nombre: "",
    precioUnitario: "",
    stock: "",
  };

  const [articulo, setArticulo] = useState(articuloDefault);
  const [mensaje, setMensaje] = useState(null);
  const [edicion, setEdicion] = useState(false);

  useEffect(() => {
    if (articuloActual !== null) {
      setArticulo({
        ...articuloActual,
      });
      setEdicion(true);
    } else {
      setArticulo(articuloDefault);
      setEdicion(false);
    }

    // eslint-disable-next-line
  }, [articuloActual]);

  const handleOnSubmit = (e) => {
    e.preventDefault();

    console.log(articulo)
    console.log(articuloActual)

    if (
      articulo.codigo.trim() === "" ||
      articulo.nombre.trim() === "" ||
      articulo.precioUnitario.trim() === ""
    ) {
      setMensaje("El código, nombre y precioUnitario no pueden estar vacíos");
      return;
    }
    if (articuloActual !== null) {
      actualizarArticulo(articulo);
    } else {
      registrarArticulo(articulo);
    }

    cerrarModal();
  };

  const limpiarFormulario = () => {
    setMensaje(null);
    setArticulo(articuloDefault);
  };
  const handleChange = (e) => {
    setArticulo({
      ...articulo,
      [e.target.name]: e.target.value,
    });
  };

  const cerrarModal = () => {
    limpiarFormulario();
    setShowModal(false);
    setModalTitle("");
    obtenerArticulo(null);
  };

  return (
    <form onSubmit={handleOnSubmit}>
      {mensaje ? <div className="notification is-danger">{mensaje}</div> : null}
      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Código</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              {edicion ? (
                <input
                  autoComplete="off"
                  className="input"
                  type="text"
                  placeholder="Código"
                  name="codigo"
                  readOnly
                  value={articulo.codigo}
                  onChange={handleChange}
                />
              ) : (
                <input
                  autoComplete="off"
                  className="input"
                  type="text"
                  placeholder="Código"
                  name="codigo"
                  value={articulo.codigo}
                  onChange={handleChange}
                />
              )}
              <span className="icon is-small is-left">
                <i className="fas fa-bars"></i>
              </span>
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Nombre</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Nombre"
                name="nombre"
                value={articulo.nombre}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-clipboard-list"></i>
              </span>
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Precio Unitario</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Precio Unitario"
                name="precioUnitario"
                value={articulo.precioUnitario}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-dollar-sign"></i>
              </span>
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Stock</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Stock"
                name="stock"
                value={articulo.stock}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-dollar-sign"></i>
              </span>
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label"></div>
        <div className="field-body">
          <div className="field">
            <div className="control">
              <button type="submit" className="button is-primary mr-1">
                Guardar
              </button>
              <button
                type="button"
                className="button"
                onClick={() => cerrarModal()}
              >
                Cancelar
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default FormularioArticulo;
