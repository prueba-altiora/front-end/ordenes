import React, { useContext } from "react";
import { ArticuloContext } from "../../contexts/ArticuloContext";
import { ModalContext } from "../../contexts/ModalContext";

const FilaArticulo = ({ articulo }) => {

    const {setModalTitle, setShowModal}= useContext(ModalContext)
    const {obtenerArticulo,eliminarArticulo} = useContext(ArticuloContext)

    const abrirModalEditarArticulo =() =>{
        obtenerArticulo(articulo);
          setModalTitle('Editar Articulo');
          setShowModal(true);
      }

  return (
    <tr>
      <td>{articulo.codigo}</td>
      <td>{articulo.nombre}</td>
      <td>{articulo.precioUnitario}</td>
      <td>{articulo.stock}</td>
      <td>
        <button className="button is-small is-info mr-1" onClick={()=>abrirModalEditarArticulo()}>
          <span className="icon is-small">
            <i className="fas fa-edit"></i>
          </span>
        </button>
      </td>
      <td>
        <button className="button is-small is-danger" onClick={()=>eliminarArticulo(articulo.id)}>
          <span className="icon is-small">
            <i className="fas fa-trash"></i>
          </span>
        </button>
      </td>
    </tr>
  );
};

export default FilaArticulo;
