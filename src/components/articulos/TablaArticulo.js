import React, { useContext, useEffect } from "react";
import { ArticuloContext } from "../../contexts/ArticuloContext";
import FilaArticulo from "./FilaArticulo";

const TablaArticulo = () => {
  const { articulosList, obtenerArticulos } = useContext(ArticuloContext);

  useEffect(() => {
    obtenerArticulos();
    // eslint-disable-next-line
  }, []);

  if (articulosList.length === 0)
    return (
      <center>
        <p>No existen Articulos</p>
      </center>
    );
  return (
    <div className="table-container">
      <table className="table is-haverable is-fullwidth">
        <thead>
          <tr>
            <th>Código</th>
            <th>Nombre</th>
            <th>Precio Unitario</th>
            <th>Stock</th>
            <th>Editar</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
          {articulosList.map((articulo) => (
            <FilaArticulo articulo={articulo} key={articulo.idArticulo} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TablaArticulo;
