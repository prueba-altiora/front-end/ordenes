import React from 'react';
import { Link } from 'react-router-dom';


const Menu = () => {
    return (  
        <nav className="panel">
            <p className="panel-heading">Menú</p>
            <div className="panel-block">
                <Link to="/" className="button is-fullwidth">
                    <span className="icon">
                    <i className="fas fa-home"/>
                    </span>
                    <span>Inicio</span>                    
                </Link>
            </div>
            <div className="panel-block">
                <Link to="/clientes" className="button is-fullwidth">
                    <span className="icon">
                    <i className="fas fa-user"/>
                    </span>
                    <span>Clientes</span>                    
                </Link>
            </div>
            <div className="panel-block">
                <Link to="/articulos" className="button is-fullwidth">
                    <span className="icon">
                    <i className="fas fa-box-open"></i>
                    </span>
                    <span>Artículos</span>                    
                </Link>
            </div>
            <div className="panel-block">
                <Link to="/ordenes" className="button is-fullwidth">
                    <span className="icon">
                    <i className="fas fa-cart-plus"></i>
                    </span>
                    <span>Crear Órdenes</span>                    
                </Link>
            </div>
            <div className="panel-block">
                <Link to="/verOrdenes" className="button is-fullwidth">
                    <span className="icon">
                    <i className="fas fa-eye"></i>
                    </span>
                    <span>Ver Órdenes</span>                    
                </Link>
            </div>
        </nav>
    );
}
 
export default Menu;