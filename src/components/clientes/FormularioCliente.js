import React, { useContext, useState, useEffect } from "react";
import { ClienteContext } from "../../contexts/ClienteContext";
import { ModalContext } from "../../contexts/ModalContext";

const FormularioCliente = () => {
  const { setShowModal, setModalTitle } = useContext(ModalContext);
  const { registrarCliente, clienteActual, obtenerCliente, actualizarCliente } =
    useContext(ClienteContext);

  const clienteDefault = {
    identificacion: "",
    nombres: "",
    apellidos: "",
  };

  const [cliente, setCliente] = useState(clienteDefault);
  const [mensaje, setMensaje] = useState(null);
  const [edicion, setEdicion] = useState(false);

  useEffect(() => {
    if (clienteActual !== null) {
      setCliente({
        ...clienteActual,
      });
      setEdicion(true);
    } else {
      setCliente(clienteDefault);
      setEdicion(false);
    }

    // eslint-disable-next-line
  }, [clienteActual]);

  const handleOnSubmit = (e) => {
    e.preventDefault();

    if (
      cliente.identificacion.trim() === "" ||
      cliente.nombres.trim() === "" ||
      cliente.apellidos.trim() === ""
    ) {
      setMensaje(
        "La identificación, los nombres y apellidos no pueden estar vacíos"
      );
      return;
    }
    if (clienteActual !== null) {
      actualizarCliente(cliente);
    } else {
      registrarCliente(cliente);
    }

    cerrarModal();
  };

  const limpiarFormulario = () => {
    setMensaje(null);
    setCliente(clienteDefault);
  };
  const handleChange = (e) => {
    setCliente({
      ...cliente,
      [e.target.name]: e.target.value,
    });
  };

  const cerrarModal = () => {
    limpiarFormulario();
    setShowModal(false);
    setModalTitle("");
    obtenerCliente(null);
  };

  return (
    <form onSubmit={handleOnSubmit}>
      {mensaje ? <div className="notification is-danger">{mensaje}</div> : null}
      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Identificación</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              {edicion ? (
                <input
                  autoComplete="off"
                  className="input"
                  type="text"
                  placeholder="Identificación"
                  name="identificacion"
                  readOnly
                  value={cliente.identificacion}
                  onChange={handleChange}
                />
              ) : (
                <input
                  autoComplete="off"
                  className="input"
                  type="text"
                  placeholder="Identificación"
                  name="identificacion"
                  value={cliente.identificacion}
                  onChange={handleChange}
                />
              )}
              <span className="icon is-small is-left">
                <i className="fas fa-id-card"></i>
              </span>
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Nombre Completo</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Nombre"
                name="nombres"
                value={cliente.nombres}
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-user"></i>
              </span>
            </p>
          </div>
          <div className="field">
            <p className="control is-expanded">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Apellidos"
                name="apellidos"
                value={cliente.apellidos}
                onChange={handleChange}
              />
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label"></div>
        <div className="field-body">
          <div className="field">
            <div className="control">
              <button type="submit" className="button is-primary mr-1">
                Guardar
              </button>
              <button
                type="button"
                className="button"
                onClick={() => cerrarModal()}
              >
                Cancelar
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default FormularioCliente;
