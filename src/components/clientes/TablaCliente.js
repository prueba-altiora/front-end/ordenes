import React, { useContext, useEffect } from "react";
import FilaCliente from "./FilaCliente";
import { ClienteContext } from "../../contexts/ClienteContext";

const TablaCliente = () => {
  const { clientesList, obtenerClientes } = useContext(ClienteContext);

  useEffect(() => {
    obtenerClientes();
    // eslint-disable-next-line
  }, []);

  if (clientesList.length === 0)
    return (
      <center>
        <p>No existen Clientes</p>
      </center>
    );

  return (
    <div className="table-container">
      <table className="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Identificación</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Editar</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
          {clientesList.map((cliente) => (
            <FilaCliente cliente={cliente} key={cliente.identificacion} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TablaCliente;
