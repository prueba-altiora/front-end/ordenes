import React, { useContext } from "react";
import { ClienteContext } from "../../contexts/ClienteContext";
import { ModalContext } from "../../contexts/ModalContext";

const BotonCrearCliente = () => {

    const {setModalTitle, setShowModal}= useContext(ModalContext)

    const {obtenerCliente} = useContext(ClienteContext)
    const abrirModalCrearCliente = () =>{
        setModalTitle('Nuevo Cliente');
        setShowModal(true);
        obtenerCliente(null);

    }

  return (
    <div className="container">
      <button className="button is-small is-primary" onClick={()=>abrirModalCrearCliente()}>
        <span className="icon is-small">
          <i className="fas fa-plus-square"></i>
        </span>
        <span>Registrar Cliente</span>
      </button>
    </div>
  );
};

export default BotonCrearCliente;
