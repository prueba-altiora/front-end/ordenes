import React, { useContext } from "react";
import { ClienteContext } from "../../contexts/ClienteContext";
import { ModalContext } from "../../contexts/ModalContext";

const FilaCliente = ({cliente}) => {

    const {setModalTitle, setShowModal}= useContext(ModalContext)
    const {obtenerCliente,eliminarCliente} = useContext(ClienteContext)

    const abrirModalEditarCliente =() =>{
      obtenerCliente(cliente);
        setModalTitle('Editar Cliente');
        setShowModal(true);
    }

  return (
    <tr>
      <td>{cliente.identificacion}</td>
      <td>{cliente.nombres}</td>
      <td>{cliente.apellidos}</td>
      <td>
        <button className="button is-small is-info mr-1" onClick={()=>abrirModalEditarCliente()}>
          <span className="icon is-small">
            <i className="fas fa-edit"></i>
          </span>
        </button>
      </td>
      <td>
        <button className="button is-small is-danger" onClick={()=>eliminarCliente(cliente.identificacion)}>
          <span className="icon is-small">
            <i className="fas fa-trash"></i>
          </span>
        </button>
      </td>
    </tr>
  );
};

export default FilaCliente;
