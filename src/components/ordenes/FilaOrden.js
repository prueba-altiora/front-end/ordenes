import React, { useContext } from "react";
import { OrdenContext } from "../../contexts/OrdenContext";
import { ModalContext } from "../../contexts/ModalContext";

const FilaOrden = ({ orden }) => {
  const { setModalTitle, setShowModal } = useContext(ModalContext);
  const { obtenerOrden } = useContext(OrdenContext);

  const abrirModalDetallesOrden = () => {
    obtenerOrden(orden);
    setModalTitle("Detalles Orden");
    setShowModal(true);
  };

  return (
    <tr>
      <td>{orden.idOrden}</td>
      <td>{orden.fecha.split("T")[0]}</td>
      <td>{orden.nombreCliente}</td>
      <td>
        <center>
          <button
            className="button is-small is-info mr-1"
            onClick={() => abrirModalDetallesOrden()}
          >
            <span className="icon is-small">
              <i className="fas fa-eye"></i>
            </span>
          </button>
        </center>
      </td>
    </tr>
  );
};

export default FilaOrden;
