import axios from "axios";
import React, { useState, useEffect } from "react";

import Autosuggest from "react-autosuggest";
import Swal from "sweetalert2";

const getSuggestions = (value, clientes) => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  return inputLength === 0
    ? []
    : clientes.filter(
        (lang) =>
          lang.nombres.toLowerCase().slice(0, inputLength) === inputValue ||
          lang.apellidos.toLowerCase().slice(0, inputLength) === inputValue
      );
};

const getSuggestionValue = (suggestion) =>
  suggestion.nombres + " " + suggestion.apellidos;

const renderSuggestion = (suggestion) => (
  <div>{suggestion.nombres + " " + suggestion.apellidos}</div>
);

const BusquedaCliente = ({ onSelect }) => {
  const [value, setValue] = useState("");
  const [cliente, setCliente] = useState([]);
  const [clienteInicial, setClienteInicial] = useState([]);

  useEffect(() => {
    axios
      .get("/cliente/listar")
      .then((response) => {
        setClienteInicial(response.data);
      })
      .catch((error) => {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "No se pudo obtener los clientes",
          toast: true,
        });
      });
  }, []);

  const onChange = (event, { newValue }) => {
    setValue(newValue);
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    setCliente(getSuggestions(value, clienteInicial));
  };

  const onSuggestionSelected = (event, { suggestion }) => {
    onSelect(suggestion.identificacion);
  };

  const inputProps = {
    placeholder: "Cliente",
    value,
    onChange: onChange,
  };

  return (
    <div className="panel">
      <div className="modal-backgroud">
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Buscar Cliente</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control is-expanded has-icons-left py-5">
                  <Autosuggest
                    suggestions={cliente}
                    onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                    getSuggestionValue={getSuggestionValue}
                    renderSuggestion={renderSuggestion}
                    inputProps={inputProps}
                    onSuggestionSelected={onSuggestionSelected}
                  />
                </p>
              </div>
          </div>
        </div>
      </div>
    </div>

  );
};

export default BusquedaCliente;
