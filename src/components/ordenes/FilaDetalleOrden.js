import React from 'react';

const FilaDetalleOrden = ({detalle}) => {


  return (
    <tr>
      <td>{detalle.nombreArticulo}</td>
      <td>{detalle.cantidad}</td>
      <td>{detalle.precioUnitario}</td>
      <td>{detalle.total}</td>
    </tr>
  );
};

export default FilaDetalleOrden;
