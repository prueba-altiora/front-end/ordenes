import axios from "axios";
import React, { useState, useEffect } from "react";

import Autosuggest from "react-autosuggest";
import Swal from "sweetalert2";
import TablaDetalles from "./TablaDetalle";
import ClientOrden from "../../middlewares/ClientOrden";

const getSuggestions = (value, articulos) => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  return inputLength === 0
    ? []
    : articulos.filter(
        (lang) => lang.nombre.toLowerCase().slice(0, inputLength) === inputValue
      );
};

const getSuggestionValue = (suggestion) => suggestion.nombre;

const renderSuggestion = (suggestion) => <div>{suggestion.nombre}</div>;

const Detalles = ({ identificacionCliente }) => {
  const [value, setValue] = useState("");
  const [articulo, setArticulo] = useState([]);
  const [articuloInicial, setArticuloInicial] = useState([]);

  const [listaArticulos, setListaArticulos] = useState([]);

  const articuloSeleccionadoDefault = {
    idArticulo: 0,
    codigo: "",
    nombre: "",
    precioUnitario: 0,
    cantidad: 1,
    isSelect: false,
  };

  const [articuloSelecionado, setArticuloSelecionado] = useState(
    articuloSeleccionadoDefault
  );


  useEffect(() => {
    axios
      .get("/articulo/listar")
      .then((response) => {
        setArticuloInicial(response.data);
      })
      .catch((error) => {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "No se pudo obtener los articulos",
          toast: true,
        });
      });
  }, []);

  const onChange = (event, { newValue }) => {
    setValue(newValue);
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    setArticulo(getSuggestions(value, articuloInicial));
  };

  const onSuggestionSelected = (event, { suggestion }) => {
    console.log(suggestion.idArticulo);
    setArticuloSelecionado({ ...suggestion, cantidad: 1, isSelect: true });
  };

  const inputProps = {
    placeholder: "Articulo",
    value,
    onChange: onChange,
  };

  const agregarDetalle = () => {
    setListaArticulos([...listaArticulos, articuloSelecionado]);
    setArticuloSelecionado(articuloSeleccionadoDefault);
  };

  const leerCantidad = (event) => {
    setArticuloSelecionado({
      ...articuloSelecionado,
      cantidad: event.target.value,
    });
  };

  const almacenarOrden = () => {
    if (identificacionCliente === "") {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Debe seleccionar un cliente para generar la orden",
        toast: true,
      });
      return;
    }

    if (listaArticulos.length === 0) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Debe seleccionar al menos un articulo para la orden",
        toast: true,
      });
      return;
    }

    const solicitudOrden = {
      fecha: new Date(),
      idCliente: identificacionCliente,
      detalles: listaArticulos,
    };
    ClientOrden.post("/orden/crear", solicitudOrden)
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "Creado",
          text: "Orden creada Exitosamente",
          toast: true,
        });
      })
      .catch((error) => {
        console.log(error)
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error.response.data.apierror.mensajeSistema,
          toast: true,
        });
      });
  };

  return (
    <>
      <div className="panel">
        <div className="modal-backgroud">
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Buscar Artículo</label>
            </div>
            <div className="field-body">
              <div className="field">
                <p className="control is-expanded has-icons-left py-5">
                  <Autosuggest
                    suggestions={articulo}
                    onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                    getSuggestionValue={getSuggestionValue}
                    renderSuggestion={renderSuggestion}
                    inputProps={inputProps}
                    onSuggestionSelected={onSuggestionSelected}
                  />
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Código Artículo</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Código Articulo"
                name="codigo"
                value={articuloSelecionado.codigo}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-bars"></i>
              </span>
            </p>
          </div>
          <div className="field-label is-normal">
            <label className="label">Nombre</label>
          </div>
          <div className="field">
            <p className="control is-expanded has-icons-left mr-2">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Nombre"
                name="nombre"
                value={articuloSelecionado.nombre}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-clipboard-list"></i>
              </span>
            </p>
          </div>
        </div>
      </div>

      <div className="field is-horizontal">
        <div className="field-label is-normal">
          <label className="label">Precio Unitario</label>
        </div>
        <div className="field-body">
          <div className="field">
            <p className="control is-expanded has-icons-left">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Precio Unitario"
                name="precio"
                value={articuloSelecionado.precioUnitario}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-dollar-sign"></i>
              </span>
            </p>
          </div>
          <div className="field-label is-normal">
            <label className="label">Cantidad</label>
          </div>
          <div className="field">
            <p className="control is-expanded has-icons-left mr-2">
              <input
                autoComplete="off"
                className="input"
                type="text"
                placeholder="Cantidad"
                name="cantidad"
                value={articuloSelecionado.cantidad}
                onChange={leerCantidad}
              />
              <span className="icon is-small is-left">
                <i class="fas fa-cart-plus"></i>
              </span>
            </p>
          </div>
        </div>
      </div>

      <div className="container">
        <center>
          <button
            className="button is-small is-primary has-background-link-dark mx-4 my-4"
            onClick={agregarDetalle}
          >
            <span className="icon is-small">
              <i className="fas fa-plus-square"></i>
            </span>
            <span>Agregar Detalle</span>
          </button>
        </center>
      </div>

      <TablaDetalles listaArticulos={listaArticulos}></TablaDetalles>

      <div className="container">
        <center>
          <button
            className="button is-small is-primary has-background-success-dark mx-4 my-4"
            onClick={almacenarOrden}
          >
            <span className="icon is-small">
              <i className="fas fa-plus-square"></i>
            </span>
            <span>Crear Orden</span>
          </button>
        </center>
      </div>
    </>
  );
};

export default Detalles;
