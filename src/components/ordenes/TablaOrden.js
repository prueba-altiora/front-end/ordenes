import React, { useContext, useEffect } from "react";
import FilaOrden from "./FilaOrden";
import { OrdenContext } from "../../contexts/OrdenContext";

const TablaOrden = () => {
  const { ordenesList, obtenerOrdenes } = useContext(OrdenContext);

  useEffect(() => {
    obtenerOrdenes();
    // eslint-disable-next-line
  }, []);

  if (ordenesList.length === 0)
    return (
      <center>
        <p>No existen Ordenes</p>
      </center>
    );

  return (
    <div className="table-container">
      <table className="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Id Artículo</th>
            <th>Fecha</th>
            <th>Nombres del Cliente</th>
            <th>Visualizar Detalles</th>
          </tr>
        </thead>
        <tbody>
          {ordenesList.map((orden) => (
            <FilaOrden orden={orden} key={orden.idArticulo} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TablaOrden;