import React, { useContext, useState, useEffect } from "react";
import { OrdenContext } from "../../contexts/OrdenContext";
import { ModalContext } from "../../contexts/ModalContext";
import FilaDetalleOrden from "./FilaDetalleOrden";

const FormularioDetalle = () => {
  const { setShowModal, setModalTitle } = useContext(ModalContext);
  const {  ordenActual, obtenerOrdenes} =
    useContext(OrdenContext);

  const ordenDefault = {
    idOrden: "",
    fecha: new Date(),
    nombreCliente: "",
    detalles: []
  };

  const [orden, setOrden] = useState(ordenDefault);
  const [mensaje, setMensaje] = useState(null);

  useEffect(() => {
    if (ordenActual !== null) {
      setOrden({
        ...ordenActual,
      });
    } else {
      setOrden(ordenDefault);
    }

    // eslint-disable-next-line
  }, [ordenActual]);

  const handleOnSubmit = (e) => {
    e.preventDefault();
    cerrarModal();
  };

  const limpiarFormulario = () => {
    setMensaje(null);
    setOrden(ordenDefault);
  };

  const cerrarModal = () => {
    limpiarFormulario();
    setShowModal(false);
    setModalTitle("");
    obtenerOrdenes(null);
  };
  return (
    <form onSubmit={handleOnSubmit}>
      {mensaje ? <div className="notification is-danger">{mensaje}</div> : null}
      <div className="table-container">
      <table className="table is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Nombre Artículo</th>
            <th>Cantidad</th>
            <th>Precio Unitario</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {orden.detalles.map((detalle) => (
            <FilaDetalleOrden detalle={detalle} key={detalle.idDetalle} />
          ))}
        </tbody>
      </table>
    </div>

      <div className="field is-horizontal">
        <div className="field-label"></div>
        <div className="field-body">
          <div className="field">
            <div className="control">
              <button
                type="button"
                className="button"
                onClick={() => cerrarModal()}
              >
                Cancelar
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default FormularioDetalle;
