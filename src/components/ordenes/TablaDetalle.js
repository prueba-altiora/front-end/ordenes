import React from 'react';
import FilaDetalle from './FilaDetalle';

const TablaDetalles = ({listaArticulos=[]}) => {
    
    return ( 
        <div className="table-container">
      <table className="table is-haverable is-fullwidth">
        <thead>
          <tr>
            <th>Código</th>
            <th>Nombre</th>
            <th>Precio Unitario</th>
            <th>Cantidad</th>
          </tr>
        </thead>
        <tbody>
          {listaArticulos.map((articulo) => (
            <FilaDetalle articulo={articulo} key={articulo.idArticulo} />
          ))}
        </tbody>
      </table>
    </div>
     );
}
 
export default TablaDetalles;