import React from "react";

const FilaDetalle = ({ articulo }) => {
  return (
    <tr>
      <td>{articulo.codigo}</td>
      <td>{articulo.nombre}</td>
      <td>{articulo.precioUnitario}</td>
      <td>{articulo.cantidad}</td>
    </tr>
  );
};

export default FilaDetalle;
