# Front End para gestionar ordenes

_Proyecto para la administraci贸n de clientes y articulos_
_La URL para poder ver el proyecto front end es:_
```sh
   http://localhost:3000/
   ```

## Comenzando 馃殌

_Estas instrucciones permitir谩n obtener una copia del proyecto en funcionamiento en su m谩quina local para prop贸sitos poder realizar la revisi贸n._
1. Clonar el repositorio
```sh
   git clone https://gitlab.com/prueba-altiora/front-end/ordenes.git
   ```

### Pre-requisitos 馃搵

_Que cosas que se necesitan para correr el software_

1. Clonar y correr el proyecto que se encuentra en el repositorio en la carpeta BackEnd llamado admin.
```sh
   git clone https://gitlab.com/prueba-altiora/backend/admin.git
   ```

2. 1. Clonar y correr el proyecto que se encuentra en el repositorio en la carpeta BackEnd llamado repo.
```sh
   git clone https://gitlab.com/prueba-altiora/backend/repo.git
   ```

3. Si a el proyecto del BackEnd admin cambió el puerto donde corre, cambiar en el archivo .env de este proyecto las URL para que puedan consumir las Apis el Front End 
```
REACT_APP_API_BASE_URL=http://localhost:9002
```

4. Si a el proyecto del BackEnd repo cambió el puerto donde corre, cambiar en el archivo ClientOrden que se encuentra en la carpeta middlewares de este proyecto las URL para que puedan consumir las Apis el Front End 
```
baseURL: `http://localhost:9003`
```
5. Tener disponible el puerto 3000, ya que la aplicaci贸n correra en ese puerto

6. Tener instalado Visual Studio Code para poder correr la aplicaci贸n.
7. Tener instalado Node.
```
Se lo puede descargar en https://nodejs.org/es/download/
```
8. Cargar el proyecto en Visual Studio Code. 
9. Ejecutar el proyecto.


## Autor 鉁掞笍


* **Jefferson Esteban Yaguana Montero** 


